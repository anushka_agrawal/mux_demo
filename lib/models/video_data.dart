class VideoData {
  Data data;

  VideoData({this.data});

  VideoData.fromJson(Map<String, dynamic> json) {
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class Data {
  bool test;
  String streamKey;
  String status;
  int reconnectWindow;
  List<PlaybackIds> playbackIds;
  NewAssetSettings newAssetSettings;
  String id;
  String createdAt;

  Data(
      {this.test,
      this.streamKey,
      this.status,
      this.reconnectWindow,
      this.playbackIds,
      this.newAssetSettings,
      this.id,
      this.createdAt});

  Data.fromJson(Map<String, dynamic> json) {
    test = json['test'];
    streamKey = json['stream_key'];
    status = json['status'];
    reconnectWindow = json['reconnect_window'];
    if (json['playback_ids'] != null) {
      playbackIds = new List<PlaybackIds>();
      json['playback_ids'].forEach((v) {
        playbackIds.add(new PlaybackIds.fromJson(v));
      });
    }
    newAssetSettings = json['new_asset_settings'] != null
        ? new NewAssetSettings.fromJson(json['new_asset_settings'])
        : null;
    id = json['id'];
    createdAt = json['created_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['test'] = this.test;
    data['stream_key'] = this.streamKey;
    data['status'] = this.status;
    data['reconnect_window'] = this.reconnectWindow;
    if (this.playbackIds != null) {
      data['playback_ids'] = this.playbackIds.map((v) => v.toJson()).toList();
    }
    if (this.newAssetSettings != null) {
      data['new_asset_settings'] = this.newAssetSettings.toJson();
    }
    data['id'] = this.id;
    data['created_at'] = this.createdAt;
    return data;
  }
}

class PlaybackIds {
  String policy;
  String id;

  PlaybackIds({this.policy, this.id});

  PlaybackIds.fromJson(Map<String, dynamic> json) {
    policy = json['policy'];
    id = json['id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['policy'] = this.policy;
    data['id'] = this.id;
    return data;
  }
}

class NewAssetSettings {
  List<String> playbackPolicies;

  NewAssetSettings({this.playbackPolicies});

  NewAssetSettings.fromJson(Map<String, dynamic> json) {
    playbackPolicies = json['playback_policies'].cast<String>();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['playback_policies'] = this.playbackPolicies;
    return data;
  }
}
