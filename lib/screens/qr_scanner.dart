import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:mux_with_qr/screens/live_video.dart';
import 'package:qr_code_scanner/qr_code_scanner.dart';

class QRScannerScreen extends StatefulWidget {
  @override
  _QRScannerScreenState createState() => _QRScannerScreenState();
}

class _QRScannerScreenState extends State<QRScannerScreen> {
  bool hasScanned;
  final GlobalKey qrKey = GlobalKey(debugLabel: 'QR');
  var qrText = "";
  QRViewController qrController;

  @override
  void initState() {
    hasScanned = false;
    super.initState();
  }

  @override
  void dispose() {
    qrController?.dispose();
    super.dispose();
  }

  void _onQRViewCreated(QRViewController controller) {
    this.qrController = qrController;
    controller.scannedDataStream.listen((scanData) async {
      if (!hasScanned) {
        hasScanned = true;
        qrText = scanData;
        print("here url is https://stream.mux.com/$qrText.m3u8");

        Navigator.of(context).push(
          MaterialPageRoute(
              builder: (context) =>
                  VideoUI('https://stream.mux.com/$qrText.m3u8')),
        );
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: <Widget>[
          Expanded(
            flex: 5,
            child: QRView(
              key: qrKey,
              onQRViewCreated: _onQRViewCreated,
            ),
          ),
          Expanded(
            flex: 1,
            child: Center(
              child: Text('Scan result: $qrText'),
            ),
          )
        ],
      ),
    );
  }
}
