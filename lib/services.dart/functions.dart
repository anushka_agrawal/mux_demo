import 'dart:convert';
import 'dart:io';
import 'package:http/http.dart' as http;
import 'package:image_picker/image_picker.dart';
import 'package:mux_with_qr/models/video_data.dart';

Future<VideoData> getVideoID() async {
  var url = 'https://api.mux.com/video/v1/live-streams';
  var response = await http.post(
    url,
    body: jsonEncode({
      "playback_policy": "public",
      "new_asset_settings": {
        "playback_policy": "public",
      }
    }),
    headers: {
      "Content-Type": "application/json",
      "Authorization": getAuthString(
        "f6ee3b38-1d2e-4fbd-b60f-65a21dd61ce1",
        "A0PFoQJCQiCH+p23XUhXUFZrRBtp5Y73NdP3hRcO3lyMCLd3NhDrVReM+JXokAJfnQZXfWQ9v13",
      )
    },
  );
  print('Response status: ${response.statusCode}');
  print('Response body: ${response.body.toString()}');
  VideoData data = VideoData.fromJson(jsonDecode(response.body));
  return data;
}

String getAuthString(String username, String password) {
  final token = base64.encode(latin1.encode('$username:$password'));

  final authstr = 'Basic ' + token.trim();

  return authstr;
}

Future<void> pickVideo() async {
  PickedFile file = await ImagePicker().getVideo(source: ImageSource.gallery);
  File video = File(file.path);
  await muxDirectUpload(video);
}

muxDirectUpload(File file) async {
  var url = 'https://api.mux.com/video/v1/uploads';
  var response = await http.post(
    url,
    body: jsonEncode({
      "new_asset_settings": {
        "playback_policy": "public",
      }
    }),
    headers: {
      "Content-Type": "application/json",
      "Authorization": getAuthString(
        "f6ee3b38-1d2e-4fbd-b60f-65a21dd61ce1",
        "A0PFoQJCQiCH+p23XUhXUFZrRBtp5Y73NdP3hRcO3lyMCLd3NhDrVReM+JXokAJfnQZXfWQ9v13",
      )
    },
  );

  String authUrl = jsonDecode(response.body)['data']['url'];

  var postUri = Uri.parse(authUrl);
  HttpClient client = HttpClient(context: SecurityContext.defaultContext);
  HttpClientRequest xyz = await client.putUrl(postUri);
  await xyz.addStream(file.openRead());
  await xyz.flush();
  final httpresponse = await xyz.close();
  print("codexxx => ${httpresponse.statusCode}");
  print("bodyxxx => ${httpresponse.contentLength}");
}
