import 'package:flutter/material.dart';
import 'package:mux_with_qr/screens/qr_scanner.dart';
import 'package:mux_with_qr/screens/upload.dart';

class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            RaisedButton(
              child: Text('Play'),
              onPressed: () async {
                Navigator.of(context)
                    .push(MaterialPageRoute(builder: (c) => QRScannerScreen()));
              },
            ),
            RaisedButton(
              child: Text('Upload'),
              onPressed: () async {
                Navigator.of(context)
                    .push(MaterialPageRoute(builder: (c) => UploadUI()));
              },
            ),
          ],
        ),
      ),
    );
  }
}
