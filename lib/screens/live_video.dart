import 'package:flutter/material.dart';
import 'package:video_player/video_player.dart';

class VideoUI extends StatefulWidget {
  final String url;
  VideoUI(this.url);

  @override
  _VideoUIState createState() => _VideoUIState();
}

class _VideoUIState extends State<VideoUI> {
  VideoPlayerController videoPlayerController;
  bool isLoading;
  @override
  void initState() {
    isLoading = true;
    loadVideo();
    super.initState();
  }

  loadVideo() async {
    videoPlayerController = new VideoPlayerController.network(widget.url);
    print("******************** here");
    videoPlayerController
      ..addListener(() {
        setState(() {});
      });
    print("******************** here 2");
    await videoPlayerController.initialize();
    await videoPlayerController.play();
    print("******************** here 3");
    setState(() {
      isLoading = false;
    });
  }

  @override
  void dispose() {
    videoPlayerController?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: isLoading
            ? CircularProgressIndicator()
            : AspectRatio(
                aspectRatio: videoPlayerController.value.aspectRatio,
                child: VideoPlayer(videoPlayerController),
              ));
  }
}
