import 'package:camera_with_rtmp/camera.dart';
import 'package:flutter/material.dart';
import 'package:mux_with_qr/models/video_data.dart';
import 'package:mux_with_qr/services.dart/functions.dart';
import 'package:qr_flutter/qr_flutter.dart';

class UploadUI extends StatefulWidget {
  @override
  _UploadUIState createState() => _UploadUIState();
}

class _UploadUIState extends State<UploadUI> {
  CameraController controller;
  List<CameraDescription> cameras;
  bool isStreaming;
  bool isFrontCamera;
  bool isVideoUploading;
  String videoId;

  @override
  void initState() {
    super.initState();
    isStreaming = false;
    isFrontCamera = false;
    isVideoUploading = false;
    availableCameras().then((value) {
      cameras = value;
      controller = CameraController(cameras[0], ResolutionPreset.low);
      controller.initialize().then((_) {
        if (!mounted) {
          return;
        }
        setState(() {});
      });
    });
  }

  @override
  void dispose() async {
    videoId = '';
    controller?.dispose();
    await controller.stopVideoStreaming();
    super.dispose();
  }

  void toggleCameraView() async {
    if (controller != null) {
      await controller.dispose();
    }
    isFrontCamera = !isFrontCamera;
    CameraDescription cameraDescription;
    if (isFrontCamera) {
      cameraDescription = cameras[1];
    } else {
      cameraDescription = cameras[0];
    }
    controller = CameraController(
      cameraDescription,
      ResolutionPreset.medium,
      enableAudio: true,
      androidUseOpenGL: true,
    );

    controller.addListener(() {
      if (mounted) setState(() {});
      if (controller.value.hasError) {
        Scaffold.of(context).showSnackBar(SnackBar(
            content:
                Text('Camera error ${controller.value.errorDescription}')));
      }
    });

    try {
      await controller.initialize();
    } on CameraException catch (e) {
      Scaffold.of(context).showSnackBar(
          SnackBar(content: Text('Camera error ${e.toString()}')));
    }

    if (mounted) {
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    if (cameras == null && controller?.value?.isInitialized != true) {
      return Container();
    }

    return Scaffold(
      body: isVideoUploading
          ? Center(child: CircularProgressIndicator())
          : SingleChildScrollView(
              child: Column(
                children: [
                  SizedBox(height: 50),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 20),
                    height: 500,
                    child: AspectRatio(
                      aspectRatio: controller.value.aspectRatio,
                      child: CameraPreview(controller),
                    ),
                  ),
                  SizedBox(height: 20),
                  RaisedButton(
                    child: Text('Switch Camera'),
                    onPressed: toggleCameraView,
                  ),
                  SizedBox(height: 20),
                  RaisedButton(
                    child: Text('Upload Video'),
                    onPressed: () async {
                      setState(() {
                        isVideoUploading = true;
                      });
                      await pickVideo();
                      setState(() {
                        isVideoUploading = false;
                      });
                    },
                  ),
                  SizedBox(height: 20),
                  RaisedButton(
                    child: Text(
                        isStreaming ? 'Stop Streaming' : 'Start Streaming'),
                    onPressed: () async {
                      videoId = '';
                      setState(() {
                        isVideoUploading = true;
                      });
                      if (isStreaming) {
                        await controller.stopVideoStreaming();
                        isStreaming = false;
                      } else {
                        VideoData data = await getVideoID();
                        videoId = data.data.playbackIds[0].id;
                        await controller.startVideoStreaming(
                            // "rtmp://192.168.0.157:1935/live/mystream"
                            "rtmp://live.mux.com/app/${data.data.streamKey}");
                        isStreaming = true;
                      }
                      setState(() {
                        isVideoUploading = false;
                      });
                    },
                  ),
                  SizedBox(height: 20),
                  if (isStreaming) ...[
                    RaisedButton(
                      child: Text('Show QR'),
                      onPressed: () {
                        showDialog(
                          context: context,
                          builder: (context) {
                            return Center(
                              child: QrImage(
                                backgroundColor: Colors.white,
                                data: videoId,
                                version: QrVersions.auto,
                                size: 200.0,
                              ),
                            );
                          },
                        );
                      },
                    ),
                    SizedBox(height: 20),
                  ]
                ],
              ),
            ),
    );
  }
}
